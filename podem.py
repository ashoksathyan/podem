#!/usr/bin/python



#This will try to implement the podem algorithm .

#Status
# now need to noramlize the circuit


import sys
import re
from read_ckt import *
from ckt import *

#Variables to save the number of different gates in the ckt
nand_no=0
and_no=0
or_no=0
xor_no=0
nor_no=0
not_no=0
buffer_no=0

#Variables to save the list of ckt input and output
module_wires=[] 
wires_level={} # to save list of wires and levels
module_inp=[]
module_out=[]

#Staring list of the gate names
nand_list=[]
and_list=[]
or_list=[]
xor_list=[]
nor_list=[]
not_list=[]
buffer_list=[]

#Dictionary of input and output wires of a gate. It is indexed with gate name as the key.
input_wires={}
output_wires={}



#checking number of arguments and exiting if it fails
assert (len(sys.argv) == 2), "Please provide verilog file path"
    
filename = sys.argv[1]

#ckt_read(filename, nand_no,nor_no,not_no,xor_no,or_no,and_no,buffer_no,nand_list,and_list,or_list,xor_list,nor_list,not_list,buffer_list,input_wires,output_wires,module_wires,module_inp,module_out)
   

for i in module_wires:
    wires_level.update({i:'x'})

#assigning level 0 to input wires
for i in module_inp:
    wires_level[i]=0

#for i in input_wires:
#    for j in input_wires[i]:



##################################################################################
#Wire Renaming, Copied from Jay
p = input_wires.values()
fan_out = []
for i in range(len(p)):
  for j in range(len(p)):
    a = set(p[i]).intersection(p[j])
    if (len(a) == 1):
      if (list(a) not in fan_out): 
        fan_out.append(list(a));
k=1
for i in range(0,3):
  for j in input_wires.values():
    if ( fan_out[i][0] in j):
      j[j.index(fan_out[i][0])] = fan_out[i][0] + '_' + str(k)
      k = k + 1
  k=1        
del p;
##################################################################################

#print("Printing Wire Levels \n", wires_level);
