import re


#####################################################################
#Circuit Initailzation

#opening file

def ckt_read(filename, nand_no,nor_no,not_no,xor_no,or_no,and_no,buffer_no,nand_list,and_list,or_list,xor_list,nor_list,not_list,buffer_list,input_wires,output_wires,module_wires,module_inp,module_out):
    #filename = sys.argv[1]
    fh = open(filename,"r");
    
    #making ckt from the file
    
    for  line in fh:
        words = re.split(r'[\s ( ) ; , \n]+',line)
    
        if words[0]=='input':   module_inp=words[1:len(words)-1]
    
        elif words[0]=='output':	module_out=words[1:len(words)-1]
    
        elif words[0]=='wire':     module_wires=words[1:len(words)-1]
    
        #Finding Nand Gates
        elif words[0]=='nand':  
            nand_no=nand_no+1
            nand_list.append(words[1]);
            input_wires.update({words[1]:words[3:len(words)-1]})
            output_wires.update({words[1]:words[2]})
            
        elif words[0]=='not':  
            not_no=not_no+1
            not_list.append(words[1]);
            input_wires.update({words[1]:words[3:len(words)-1]})
            output_wires.update({words[1]:words[2]})
    
        elif words[0]=='nor':  
            nor_no=nor_no+1
            nor_list.append(words[1]);
            input_wires.update({words[1]:words[3:len(words)-1]})
            output_wires.update({words[1]:words[2]})
    
        elif words[0]=='xor':  
            xor_no=xor_no+1
            xor_list.append(words[1]);
            input_wires.update({words[1]:words[3:len(words)-1]})
            output_wires.update({words[1]:words[2]})
            	
        elif words[0]=='or':  
            or_no=or_no+1
            or_list.append(words[1]);
            input_wires.update({words[1]:words[3:len(words)-1]})
            output_wires.update({words[1]:words[2]})
    
        elif words[0]=='and':  
            and_no=and_no+1
            and_list.append(words[1]);
            input_wires.update({words[1]:words[3:len(words)-1]})
            output_wires.update({words[1]:words[2]})
    
        elif words[0]=='buffer':  
            buffer_no=buffer_no+1
            buffer_list.append(words[1]);
            input_wires.update({words[1]:words[3:len(words)-1]})
            output_wires.update({words[1]:words[2]})
    
    fh.close()
    module_wires=module_wires+module_inp+module_out

#End of Circuit Initaization
#####################################################################
