# This will define the gate class.
# Gate will have list of input wires, output wires, the level of the gate and the gate type.

from enum import Enum


gate_type = Enum('gate_type','nand and or xor nor not buffer');

class gate:



    def __init__(self,g_type,name,input_wires,output_wires):


        self.g_type=g_type
        self.name=name
        self.input_wires=input_wires
        self.output_wires=output_wires
        self.gate_level='x'

        #finding gate non controlling value (ncv)
        if self.g_type == 'nand':
            self.ncv = '1'
        elif self.g_type == 'and':
            self.ncv = '1'
        elif self.g_type == 'or':
            self.ncv = '0'
        elif self.g_type == 'nor':
            self.ncv = '0'
        else:
            self.ncv = 'x'

        #setting whether it is a inverting or non_inverting gate
        if self.g_type is 'nand' or self.g_type == 'not' or self.g_type == 'nor' :
            self.inverting = 1
        else:
            self.inverting = 0

    # need to figure out how to call this function !!
    def gate_out(self,inputs):
        if self.g_type == 'nand':
            if  (inputs[0]=='0' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='0' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='1'):return '0'
            elif(inputs[0]=='x' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='x' and inputs[1]=='1'):return 'x'
            elif(inputs[0]=='x' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='0' and inputs[1]=='x'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='x'):return 'x'

        elif self.g_type == 'and':
            if(inputs[0]=='0' and inputs[1]=='0'):return '0'
            elif(inputs[0]=='0' and inputs[1]=='1'):return '0'
            elif(inputs[0]=='1' and inputs[1]=='0'):return '0'
            elif(inputs[0]=='1' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='x' and inputs[1]=='0'):return '0'
            elif(inputs[0]=='x' and inputs[1]=='1'):return 'x'
            elif(inputs[0]=='x' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='0' and inputs[1]=='x'):return '0'
            elif(inputs[0]=='1' and inputs[1]=='x'):return 'x'

        elif self.g_type == 'or':
            if(inputs[0]=='0' and inputs[1]=='0'):return '0'
            elif(inputs[0]=='0' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='x' and inputs[1]=='0'):return 'x'
            elif(inputs[0]=='x' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='x' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='0' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='1' and inputs[1]=='x'):return '1'

        elif self.g_type == 'xor':
            if(inputs[0]=='0' and inputs[1]=='0'):return '0'
            elif(inputs[0]=='0' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='1'):return '0'
            elif(inputs[0]=='x' and inputs[1]=='0'):return 'x'
            elif(inputs[0]=='x' and inputs[1]=='1'):return 'x'
            elif(inputs[0]=='x' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='0' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='1' and inputs[1]=='x'):return 'x'

        elif self.g_type == 'xnor':
            if(inputs[0]=='0' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='0' and inputs[1]=='1'):return '0'
            elif(inputs[0]=='1' and inputs[1]=='0'):return '0'
            elif(inputs[0]=='1' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='x' and inputs[1]=='0'):return 'x'
            elif(inputs[0]=='x' and inputs[1]=='1'):return 'x'
            elif(inputs[0]=='x' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='0' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='1' and inputs[1]=='x'):return 'x'

        elif self.g_type == 'nor':
            if(inputs[0]=='0' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='0' and inputs[1]=='1'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='1' and inputs[1]=='1'):return '0'
            elif(inputs[0]=='x' and inputs[1]=='0'):return '1'
            elif(inputs[0]=='x' and inputs[1]=='1'):return '0'
            elif(inputs[0]=='x' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='0' and inputs[1]=='x'):return 'x'
            elif(inputs[0]=='1' and inputs[1]=='x'):return '0'

        elif self.g_type == 'not':
            if(inputs[0]=='0'):return '1'
            elif(inputs[0]=='1'):return '0'
            elif(inputs[0]=='x'):return 'x'

        elif self.g_type == 'buffer':
            if(inputs[0]=='0'):return '0'
            elif(inputs[0]=='1'):return '1'
            elif(inputs[0]=='x'):return 'x'
