module myckt (N1,N2,N3,N4);

input N1,N2,N3;

output N4;

wire N5,N6,N7;

not NOT1 (N5,N3);
and AND2_1 (N6,N5,N1);
and AND2_2 (N7,N2,N3);
or  OR2_1  (N4,N6,N7);
dff DFF1 (N2,N4);
