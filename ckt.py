#!/usr/bin/python

# This usues python3

#Now need to start adding logging each step into a log file.


import re
from gate import *
import sys
from collections import deque


class ckt:

	#Variables to save the number of different gates in the ckt
	nand_no=0
	and_no=0
	or_no=0
	xor_no=0
	nor_no=0
	not_no=0
	buffer_no=0

	#Variables to save the list of ckt input and output
	module_wires=[]
	wires_level={} # to save list of wires and levels
	module_inp=[]
	module_out=[]
	module_ppi=[]
	module_ppo=[]

	#dict of wires to its connected gate list
	input_wires={}
	output_wires={}


	#Staring list of the gate names
	nand_list=[]
	and_list=[]
	or_list=[]
	xor_list=[]
	xnor_list=[]
	nor_list=[]
	not_list=[]
	buffer_list=[]
	gate_list={}

	#now need to make something like a linked-list of queues. Then we will call all gates attached to the wire which has an event in that particular wire.
	#First we will do it for a combinational logic, and then build it up
	#event_queue=[]

	#this will be used to save the values of all the wires. We will init this to 'x'
	wires_value={}
	wires_value_FF={}
	wires_value_faulty={}


#####################################################################
#Circuit Initailzation

	def __init__(self,filename,flog):
		fh = open(filename,"r");

		#making ckt from the file
		line_num=1;
		for  line in fh:
			words = re.split(r'[\s ( ) ; ,  \n]+',line)

			if words[0]=='input':	self.module_inp=words[1:len(words)-1]

			elif words[0]=='output':	self.module_out=words[1:len(words)-1]

			elif words[0]=='wire':
				self.module_wires=words[1:len(words)-1]

			#Finding Gates
			elif words[0]=='nand':
				self.nand_no=self.nand_no+1
				self.nand_list.append(gate('nand',words[1],words[3:len(words)-1],words[2]))
				for i in words[3:len(words)-1]:
					if(i  in self.input_wires):
						self.input_wires[i].append(words[1])
					else:
						self.input_wires.update({i:[words[1]]})
				self.output_wires.update({words[2]:words[1]})

			elif words[0]=='not':
				self.not_no=self.not_no+1
				self.not_list.append(gate('not',words[1],words[3:len(words)-1],words[2]))
				for i in words[3:len(words)-1]:
					if(i  in self.input_wires):
						self.input_wires[i].append(words[1])
					else:
						self.input_wires.update({i:[words[1]]})
				self.output_wires.update({words[2]:words[1]})

			elif words[0]=='nor':
				self.nor_no=self.nor_no+1
				self.nor_list.append(gate('nor',words[1],words[3:len(words)-1],words[2]))
				for i in words[3:len(words)-1]:
					if(i  in self.input_wires):
						self.input_wires[i].append(words[1])
					else:
						self.input_wires.update({i:[words[1]]})
				self.output_wires.update({words[2]:words[1]})

			elif words[0]=='xor':
				self.xor_no=self.xor_no+1
				self.xor_list.append(gate('xor',words[1],words[3:len(words)-1],words[2]))
				for i in words[3:len(words)-1]:
					if(i  in self.input_wires):
						self.input_wires[i].append(words[1])
					else:
						self.input_wires.update({i:[words[1]]})
				self.output_wires.update({words[2]:words[1]})

			elif words[0]=='or':
				self.or_no=self.or_no+1
				self.or_list.append(gate('or',words[1],words[3:len(words)-1],words[2]))
				for i in words[3:len(words)-1]:
					if(i  in self.input_wires):
						self.input_wires[i].append(words[1])
					else:
						self.input_wires.update({i:[words[1]]})
				self.output_wires.update({words[2]:words[1]})

			elif words[0]=='and':
				self.and_no=self.and_no+1
				self.and_list.append(gate('and',words[1],words[3:len(words)-1],words[2]))
				for i in words[3:len(words)-1]:
					if(i  in self.input_wires):
						self.input_wires[i].append(words[1])
					else:
						self.input_wires.update({i:[words[1]]})
				self.output_wires.update({words[2]:words[1]})

			elif words[0]=='buffer':
				self.buffer_no=self.buffer_no+1
				self.buffer_list.append(gate('buffer',words[1],words[3:len(words)-1],words[2]))
				for i in words[3:len(words)-1]:
					if(i  in self.input_wires):
						self.input_wires[i].append(words[1])
					else:
						self.input_wires.update({i:[words[1]]})
				self.output_wires.update({words[2]:words[1]})

			elif words[0] == 'dff':
				self.module_ppi.append(words[2])
				self.module_ppo.append(words[3])
			elif words[0]=='\\': pass
			#else :
			#	 print("Unknown component in netlist line",line_num)
			#	 sys.exit(1)
			#line_num=line_num+1
			#print(line_num," ")
			#

		fh.close()
		#self.module_wires=self.module_wires+self.module_inp+self.module_out
		flog.write("Successfully read the ckt file\n")


		for i in self.nand_list+self.and_list+self.or_list+self.xor_list+self.nor_list+self.not_list+self.buffer_list:
			self.gate_list.update({i.name:i})

		#self.gate_list=self.nand_list+self.and_list+self.or_list+self.xor_list+self.nor_list+self.not_list+self.buffer_list
		#Now we need to levelize the ckt
		#normalizing the wires

		print(self.module_wires)
		#First assigning level 'x' to all the wires
		for i in self.module_wires+self.module_inp+self.module_out+self.module_ppi+self.module_ppo:
			self.wires_level.update({i:'x'})
		print(self.wires_level)

		#assigning level 0 to input wires
		for i in self.module_inp + self.module_ppi:
			self.wires_level[i]=0

		print(self.wires_level)
		complete ='0'
		while complete=='0':
			for k,i in self.gate_list.items():
				if i.gate_level=='x':
					#Check the level of the input wires of the gate
					temp=i.input_wires
					temp_level=0
					for j in temp:
						if self.wires_level[j]!='x' :
							if temp_level=='x':pass
							elif self.wires_level[j]>temp_level: temp_level =self.wires_level[j]
						else:
							temp_level = 'x'
							break
					i.gate_level=temp_level


		# Now that we have got the gates normalised, assign levels to the output wires for which you just got levels.
			for k,i in self.gate_list.items():
				if i.gate_level!='x': self.wires_level[i.output_wires]=i.gate_level+1
			print(self.module_ppi)
			complete='1'
			#checking if wires have been assigned a level.
			for i in self.module_wires+self.module_inp+self.module_out:
				if self.wires_level[i]=='x':
					print(i)
					complete='0'

		flog.write("Finished normalising circuit\n")
		#setting intial value of all wires to 'x'
		for i in self.module_wires+self.module_inp+self.module_out:
			self.wires_value[i]='x'


	#For this function, pass a list of the wires and its correspondin values to the function. Then this
	#function can do a event driven simulation for just these wires that have changed. Note that we can pass
	#even the internal wires to the function.
	#flog is used to pass the log file handle
	def combinational_simulation(self,inputs,flog):		#function to be used for combinational ckt simulation
		#checking whether the inputs are actually changing the wire values. Those which are really changing the wire
		#values are added to the event queue
		event_queue=[]	#[[wire,value]..]



		#checking whether we need to add input vectors to event queue
		for i in inputs:
			if(self.wires_value[i[0]]!=i[1]):
				event_queue.append(i)
		event_queue = deque(event_queue)


		#intial event queue is used to return all the wires that get changed due to a fault


		#checking if the event queue is empty. If it is not empty, go dequeue the first eleemet, and simulate all
		#the gates connected to that wire. Along with that make new events for the gate outputs.
		while event_queue:
			event=event_queue.popleft()
			if self.wires_value[event[0]]==event[1]:pass
			self.wires_value.update({event[0]:event[1]})
			#print(event[0]," ",event[1])
			flog.write("Event occured at "+event[0]+" : "+event[1]+"\n")

			#need to check whether that wire is an primary output or not before doing this
			if event[0] in self.input_wires:
				for i in self.input_wires[event[0]]: #for all gates connected to that particular wire
					gate_inputs=[]
					for j in self.gate_list[i].input_wires: #getting input vectors for that gate
						gate_inputs.append(self.wires_value[j])

					#self.gate_list[i].out(gate_inputs)
					gate_output=self.gate_list[i].gate_out(gate_inputs)
					#flog.write("Gate "+str(self.gate_list[i].name) + " Output "+ str(self.gate_list[i].output_wires) +" : "+str(gate_output)+ "\n")
					#now we need to check whether this output should be added to the event queue and add it if needed
					#if(self.wires_value[self.gate_list[i].output_wires]!=gate_output):
					event_queue.append([self.gate_list[i].output_wires,gate_output])
					#print(event_queue)

		#After event queue gets empty, we need to add the fault if its there.
		#Now copy the faultfree_wire to faulty_wires. Then do rest of the simulation on this faulty wire
		#So the best idea is to simulate the fault at the gate , and add an event to gate output if there is change
		#to event queue and make changes to faulty_wire


		#returning the output vector
		out_vector=[]
		for i in self.module_out:
			out_vector.append([i,self.wires_value[i]])
		flog.write("Events completed. Output Vector : "+str(out_vector)+"\n\n")

		return out_vector



	def find_checkpoints(self):
		#define this to find all the checkpoints in the ckt
		#checkpoints = primary_inp + fanouts
		checkpoints = []	#should i make it a local or class member?
		for i in self.input_wires:
			if (len(self.input_wires[i]) > 1):
				checkpoints.append(i)
		for i in self.module_inp:
			if i not in checkpoints:
				checkpoints.append(i)
		#should we return just the fanout points only ??
		return checkpoints

	#will find all paths from the given wire to the output. It will return a list of all paths to the primary out
	def path_to_out(self,wire_name):
		path=[]
		current_wire_name=wire_name
		#checking whether that wire is a fanout out or not

		while 1:

			path.append(current_wire_name)
			#checking whether the present wire is connected to any of the gate. This is better than checking whether its a PO.
			if current_wire_name not in  self.input_wires:
				return path
			#checking whether this wire is connected to many gates. If so we need to find multiple paths to output
			elif len(self.input_wires[current_wire_name])>1:
				#if its a fanout, then we find all the output wires connected to that gate and find paths from there
				temp=[]
				for i in self.input_wires[current_wire_name]:
					temp.append(self.path_to_out(self.gate_list[i].output_wires))
				temp_path=[]
				for i in temp:
					temp_path.append(path+i)
				return temp_path
			else :
				current_wire_name=self.gate_list[self.input_wires[current_wire_name][0]].output_wires
				#print(path)

	#all paths from one wire to another wire. wire1 should be of lower level than wire2
	#will complete it later... need to figure out when to give up.
	#one possible way is to drop one path when the current_wire_name is of higher level that wire2
	def path_wire_wire(wire1,wire2):
		path=[]
		current_wire_name=wire1
		#checking whether that wire is a fanout out or not

		while 1:

			path.append(current_wire_name)
			#checking whether the present wire is connected to any of the gate. This is better than checking whether its a PO.
			if (current_wire_name == self.input_wires):
				return path
			#checking whether this wire is connected to many gates. If so we need to find multiple paths to output
			elif len(self.input_wires[current_wire_name])>1:
				#if its a fanout, then we find all the output wires connected to that gate and find paths from there
				temp=[]
				for i in self.input_wires[current_wire_name]:
					temp.append(self.path_wire_wire(self.gate_list[i].output_wires))
				temp_path=[]
				for i in temp:
					temp_path.append(path+i)
				return temp_path
			else :
				current_wire_name=self.gate_list[self.input_wires[current_wire_name][0]].output_wires
				#print(path)



	#used to find all the primary inputs that are connected to a particular wire and no of inversions for each path
	def primary_inp(self,wire_name):
		pi={} #empty primary input list
		inverting = 0
		length = 0
		#checking if the current wire is a PI and then exiting if so
		if wire_name in self.module_inp:
			pi.update({wire_name:[inverting,length]})
			return pi

		
		#finding the gate name which is controlling this particular wire
		gate_name=self.output_wires[wire_name]
		inverting = bool(inverting)^bool( self.gate_list[gate_name].inverting )
		length=length+1
		for i in self.gate_list[gate_name].input_wires :
			temp=self.primary_inp(i)
			for j in temp:
				if j not in pi:
					pi.update({j:[bool(temp[j][0])^bool(inverting),length+temp[j][1]]})
				#updating only when longer path is found
				elif temp[j][1]>pi[j][1]:
					pi.update({j:[bool(temp[j][0])^bool(inverting),length+temp[j][1]]})
		return pi

	#fault = [wire,gate_to_which its_connected,sa0/sa1]
	def fwd_implication(self,fault):
		#find all paths possible for the fault to propagate to the output
		path_to_out=[self.path_to_out(fault[1].output_wires)] #list of all paths


	#this function will modify PI till we are able to set wire_name to specified value
	#first find all the PI in the fan-in cone of wire_name. Then we start setting values until the wire gets justified to required value
		#printing inputs of function
	def objective(self,wire_name,value1,flog):
		flog.write("Obejective is :"+ wire_name + ":" + value1 + "\n")
		flog.write("Current wire values :" + str(self.wires_value) + "\n")

		pi = self.primary_inp(wire_name)
		#find longest path
		if value1=='1': value = True
		else : value = False
		length = 0
		longest = None
		inverting = '0'
		#finding the longest path to PI which is set to 'x'
		for  i in pi:
			if (pi[i][1] >= length)  and (self.wires_value[i] == 'x'):
				longest = i
				length = pi[i][1]
				inverting = pi[i][0]
		required_pi={longest:'x'}

		#check if there is no available wire. If no wire is left untouched, then longest will be None
		if(longest== None):
			return None

		#saving value of wires before we simulate it
		stack=self.wires_value.copy();


		#checking whether we can meet the objective by setting the value to required value(ie depending on path value)

		required_pi.update({longest:str(int(bool(inverting)^bool(value)))})
		self.combinational_simulation([[longest,str(int((inverting)^(value)))]],flog)
		if self.wires_value[wire_name] == value1 :
			return required_pi
		else:
			temp = self.objective(wire_name,str(int(value)),flog)
			if temp != None :
				required_pi.update(temp)
				return required_pi


		required_pi.update({longest:str(int(~(bool(inverting)^bool(value))))})
		self.wires_value=stack.copy()

		#checking for negated value when above case `fails`
		self.combinational_simulation([[longest,str(int(not((inverting)^(value))))]],flog)
		if self.wires_value[wire_name] == value1 :
			required_pi.update({longest:str(int(~(bool(inverting)^bool(value))))})
			return required_pi
		else:
			temp = self.objective(wire_name,str(int(value)),flog)
			if temp != None :
			    required_pi.update(temp)
			    return required_pi
		return None


	#This function will propagate the fault towards the output. First we will see if the fault is sensitizable using the objective function
	#Then if it is, we will start making the D-frontier

	def propagate_fault(self,wire_name, fault,gate_name, flog):

		#seeing if the fault is sensitizable
		for i in self.wires_value:
			self.wires_value[i] = 'x'

		if fault == '1' :
			temp=self.objective(wire_name,'0' , flog)
		else:
			temp=self.objective(wire_name,'1', flog )

		if temp == None :
			flog.write("Redundant Fault " + fault + "@" + wire_name )
			return None

		self.wires_value_FF=self.wires_value.copy()


		#now injecting fault in the ckt
		if (len(self.input_wires[wire_name]) == 1):
			self.combinational_simulation([[wire_name,fault]],flog)
		else :
			#do it for output of that gate only
			if (self.gate_list[gate_name].inverting == 1 )and (fault == '0' ):
				self.combinational_simulation([[self.gate_list[gate_name].output_wires,'1']],flog)
			elif self.gate_list[gate_name].inverting == 0 and fault == '0':
				self.combinational_simulation([[self.gate_list[gate_name].output_wires,'0']],flog)
			elif self.gate_list[gate_name].inverting == 1 and fault == '1':
				self.combinational_simulation([[self.gate_list[gate_name].output_wires,'0']],flog)
			elif self.gate_list[gate_name].inverting == 0 and fault == '1':
				self.combinational_simulation([[self.gate_list[gate_name].output_wires,'1']],flog)

		self.wires_value_faulty=self.wires_value.copy()

		#self.wires_value = self.wires_value_FF.copy()

		#First we will find all wires that don't have same value in FF and fauly. Then we will see which wires have paths with uninit i/p
		temp = []

		#finding wires which have fault
		for i in self.wires_value_FF :
			if self.wires_value_FF[i] != self.wires_value_faulty[i]:
				temp.append(i)

		#checking if any of those wires are in PO
		#for i in temp :
		#	if i in self.module_out:
		#		return 1


		d_front = []
		d_front.append(gate_name)
		#now finding which gates have 'x' input
		for i in temp:
			if i in self.input_wires:
				for j in self.input_wires[i]:
					for k in self.gate_list[j].input_wires:
						if self.wires_value[k] == 'x':
							if k not in d_front:
								d_front.append(j)

		#if len(d_front) == 0:
		#	return 1;
		while(len(d_front) != 0) :
			#if we find any diff in PO ,we can stop
			#finding wires which have fault

			gate_name = d_front.pop()

			#Now propagating fault forward
			temp_inp_vectors = [] #for storing the INP Vectors of this forward propagation
			for i in self.gate_list[gate_name].input_wires:
				if self.wires_value[i] == 'x':
					#now checking to which all gates the wire is connected, and settting NCV values to its inp
					target = self.gate_list[gate_name].ncv
					#for XOR/XNOR gate, first we will try if we can meet objective of '0' else we will try with '1'
					if target == 'x':
						target = '0'
						temp_wires = self.wires_value.copy()
						temp=self.objective(i,target , flog)
						if temp == None:
							target = '1'
						self.wires_value = temp.wires.copy()

					temp=self.objective(i,target , flog)
					#now injecting fault in the ckt
					if (len(self.input_wires[wire_name]) == 1):
						self.combinational_simulation([[wire_name,fault]],flog)
					else :
						#do it for output of that gate only
						if (self.gate_list[gate_name].inverting == 1 )and (fault == '0' ):
							self.combinational_simulation([[self.gate_list[gate_name].output_wires,'1']],flog)
						elif self.gate_list[gate_name].inverting == 0 and fault == '0':
							self.combinational_simulation([[self.gate_list[gate_name].output_wires,'0']],flog)
						elif self.gate_list[gate_name].inverting == 1 and fault == '1':
							self.combinational_simulation([[self.gate_list[gate_name].output_wires,'0']],flog)
						elif self.gate_list[gate_name].inverting == 0 and fault == '1':
							self.combinational_simulation([[self.gate_list[gate_name].output_wires,'1']],flog)


					if temp == None :
						flog.write("Redundant Fault " + fault + "@" + wire_name )
						return None
					temp_inp_vectors.append(temp)



			#Now we need to give the same IP Vectors to fault free ckt
			for i in temp_inp_vectors:
				key = str(list(i)[0])
				self.wires_value_faulty = self.wires_value.copy()
				self.wires_value = self.wires_value_FF.copy()
				self.combinational_simulation([[key,i[key]]],flog)
				self.wires_value_FF = self.wires_value.copy()
				self.wires_value = self.wires_value_faulty.copy()




			#if we find any diff in PO ,we can stop
			#finding wires which have fault
			temp1 = []
			for i in self.wires_value_FF :
				if self.wires_value_FF[i] != self.wires_value_faulty[i]:
					temp1.append(i)
			#checking if any of those wires are in PO
			for i in temp1 :
				if i in self.module_out and self.wires_value_FF[i] != 'x' and self.wires_value_faulty[i] != 'x' :
					return 1
			#now finding which gates have 'x' input
			for i in temp:
				if i in self.input_wires:
					for j in self.input_wires[i]:
						for k in self.gate_list[j].input_wires:
							if self.wires_value[k] == 'x':
								if k not in d_front:
									d_front.append(j)



		#find all gates connected to the fault wire. Then assign NCV values to all the other i/ps for the these gates.
